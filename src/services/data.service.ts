import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { EngineerModel, AllocationModel } from '../models/schedule-model';

@Injectable({
  providedIn: 'root'
})

export class DataService {
  public viewAs: Subject<EngineerModel> = new Subject();
  public allocateFrom: Subject<AllocationModel> = new Subject();
  public updateMonthScheduleKeys: Subject<string[]> = new Subject();
  constructor() { }
}
