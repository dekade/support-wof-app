export enum TimeOfDay {
    am = 0,
    pm = 1
}

export class EngineerModel {
    public id: string;
    public name: string;
}

export class SupportDayModel {
    public calendarDay: Date;
    public amAssignment: EngineerModel;
    public pmAssignment: EngineerModel;
    public isBooked: boolean;
}

export class ScheduleModel {
    public id: string;
    public scheduledDayModels: [SupportDayModel];
}

export class AllocationModel {
    public calendarDate: Date;
    public engineer: EngineerModel;
    public slot: TimeOfDay;
}

export class SwapRequestModel {
    public dateFrom: Date;
    public dateTo: Date;
    public engineerFromId: string;
    public engineerToId: string;
    public sourceTimeSlot: TimeOfDay;
    public destinationTimeSlot: TimeOfDay;
}
