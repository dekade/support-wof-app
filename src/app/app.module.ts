import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'angular-calendar';

import { AppComponent } from './app.component';
import { ScheduleService } from '../services/schedule.service';
import { ScheduleComponent } from '../components/schedule/schedule.component';
import { EngineerComponent } from '../components/engineer/engineer.component';
import { DataService } from '../services/data.service';
import { SwapComponent } from '../components/swap/swap.component';



@NgModule({
  declarations: [
    AppComponent,
    ScheduleComponent,
    EngineerComponent,
    SwapComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot(),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [ScheduleService, DataService],
  bootstrap: [AppComponent, ScheduleComponent, EngineerComponent]
})
export class AppModule { }
