export const environment = {
  production: true,
  api_version: '1.0',
  api_base: 'http://localhost:50761',
  am_start_hour: 8,
  am_end_hour: 12,
  pm_start_hour: 13,
  pm_end_hour: 17,
};
